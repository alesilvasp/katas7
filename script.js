/** forEach() */

function myCallbackForEach(item, index, array) {
    return `O índice ${index} no array ${array} é: ${item}`
}

function newForEach(array, myCallback) {
    for (let i = 0; i < array.length; i++) {
        let item = array[i];
        console.log(myCallback(item, i, array))
    }
}

/** map() */

function myCallbackMap(item, index) {
    return item + 2
}

function newMap(array, myCallback) {
    let myArray = []
    for (let i = 0; i < array.length; i++) {
        let item = array[i]
        myArray.push(myCallback(item, i))
    }
    return myArray
}

/** some() */

function myCallbackSome(item, index) {
    if (item > 10) {
        return true
    }
    return false
}

function newSome(array, myCallback) {
    let output = false
    for (let i = 0; i < array.length; i++) {
        let item = array[i];
        output = myCallback(item, i)
    }
    return output
}

/** find() */

function myCallbackFind(item, index) {
    if (item > 10) {
        return item
    }
}

function newFind(array, myCallback) {
    let output = undefined
    for (let i = 0; i < array.length; i++) {
        let item = array[i];
        output = myCallback(item)
        if (output !== undefined) {
            return output
        }
    }
    return output;
}

/** findIndex() */

function myCallbackFindIndex(item, index) {
    if (item > 5) {
        return index
    }
    return -1
}

function newFindIndex(array, myCallback) {
    let output = -1
    for (let i = 0; i < array.length; i++) {
        let item = array[i]
        output = myCallback(item, i)
        if (output !== -1) {
            return output
        }
    }
    return output
}

/** every() */

function myCallbackEvery(item, index) {
    if (item > 5) {
        return true
    }
    return false
}

function newEvery(array, myCallback) {
    for (let i = 0; i < array.length; i++) {
        let item = array[i]
        if (myCallback(item, i) === false) {
            return false
        }
    }
    return true
}

/** filter() */

function myCallbackFilter(item, index) {
    if (item > 5) {
        return item
    } else {
        return false
    }
}

function newFilter(array, myCallback) {
    let newArray = []
    for (let i = 0; i < array.length; i++) {
        let item = array[i]
        if (myCallback(item) !== false) {
            newArray.push(item)
        }
    }
    return newArray
}

/** concat() */

function newConcat(array) {
    let newArrayConcat = array

    for (let j = 1; j < arguments.length; j++) {
        if (typeof arguments[j] !== 'object') {
            newArrayConcat.push(arguments[j])
        }
        else {
            for (let arg = 0; arg < arguments[j].length; arg++) {
                newArrayConcat.push(arguments[j][arg])
            }
        }
    }
    return newArrayConcat
}

/** includes() */

function newIncludes(array, element, index = 0) {
    for (let i = index; i < array.length; i++) {
        let item = array[i]
        if (element === item) {
            return true
        }
    }
    return false
}

/** indexOf() */

function newIndexOf(array, elemento, inicialIndex = 0) {
    let output = -1
    if (inicialIndex >= array.length) {
        return output
    }
    if (inicialIndex < 0) {
        for (let i = array.length; i > 0; i--) {
            let item = array[i]
            if (elemento === item) {
                output = i
                return output
            }
        }
    }
    for (let i = 0; i < array.length; i++) {
        let item = array[i]
        if (elemento === item) {
            output = i
            return output
        }
    }
    return output
}

/** join() */

function newJoin(array, separator = ',') {
    let output = ''
    if (array.length === 0) {
        return output
    }
    for (let i = 0; i < array.length; i++) {
        let item = array[i]
        output += item + separator.toString()
    }
    return output
}

/** reduce() */

function myCallbackReduce(acc, value, index, array) {
    return acc * value
}

function newReduce(array, myCallback, initialValue = 0) {
    if (initialValue > 0) {
        let output = initialValue
        for (let i = 0; i < array.length; i++) {
            let acc = output;
            let value = array[i];
            output = myCallbackReduce(acc, value)
        }
        return output
    }
    let output = array[0]
    for (let i = 1; i < array.length; i++) {
        let acc = output;
        let value = array[i];
        output = myCallbackReduce(acc, value)
    }
    return output
}


/** slice() */

function newSlice(array, start = 0, end = 0) {
    let newArray = []
    if (start > array.length) {
        return newArray
    }

    if (start < 0 && end !== 0) {
        return newArray
    }

    if (end === 0 && start >= 0) {
        for (let i = start; i < array.length; i++) {
            let item = array[i]
            newArray.push(item)
        }
        return newArray
    }

    if (start < 0) {
        for (let i = array.length + start; i < array.length; i++) {
            let item = array[i]
            newArray.push(item)
        }
        return newArray
    }

    if (start >= 0 && end < 0) {
        for (let i = start; i < array.length + end; i++) {
            let item = array[i]
            newArray.push(item)
        }
        return newArray
    }

    if (start >= 0 && end > 0) {
        for (let i = start; i < end; i++) {
            let item = array[i]
            newArray.push(item)
        }
        return newArray
    }

}

/** flat() */

function newFlat(array, depth = 1) {
    let newArray = []

    if (depth === Infinity) {
        for (let i = 0; i < array.length; i++) {
            if (typeof array[i] !== 'object') {
                newArray.push(array[i])
            }
            else {
                newArray.push(...newFlat(array[i], Infinity))
            }
        }
        return newArray
    }

    else if (depth > 1) {
        // code here;
        return newArray
    }

    for (let i = 0; i < array.length; i++) {
        if (typeof array[i] !== 'object') {
            newArray.push(array[i])
        }
        else {
            for (let j = 0; j < array[i].length; j++) {
                newArray.push(array[i][j])
            }
        }
    }
    return newArray
}

let arr = [3, 4, [5, 6, [7, 8]]]
console.log(newFlat(arr))

/** array.of() */

function newArrayOf() {
    let newArray = []
    for (let i = 0; i < arguments.length; i++) {
        newArray.push(arguments[i])
    }
    return newArray
}

/** fill() */

function newFill(array, value, start = 0, end = 0) {
    if (start === 0 && end === 0) {
        for (let i = 0; i < array.length; i++) {
            array[i] = value
        }
        return array
    }
    if (start < 0 && end === 0) {
        for (let i = array.length + start; i < array.length; i++) {
            array[i] = value
        }
        return array
    }
    if (start > 0 && end > 0) {
        for (let i = start; i < end; i++) {
            array[i] = value
        }
        return array
    }
    if (start < 0 && end < 0) {
        for (let i = array.length + start; i < array.length + end; i++) {
            array[i] = value
        }
        return array
    }
    if (end < 0) {
        for (let i = start; i < array.length + end; i++) {
            array[i] = value
        }
        return array
    }
}